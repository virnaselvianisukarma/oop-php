<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title></title>
</head>
<body>
	<?php

	require_once('frog.php');
	require_once('ape.php');

	$object = new animal("shaun");

	echo "Name: " . $object->name . "<br>";
	echo "Legs: " . $object->legs . "<br>";
	echo "Cold Blooded: " . $object->cold_blooded . "<br><br>";

	$kodok = new frog("Buduk");

	echo "Name: " . $kodok->name . "<br>";
	echo "Legs: " . $kodok->legs . "<br>";
	echo "Cold Blooded: " . $kodok->cold_blooded . "<br>";
	echo "Jump: " . $kodok->jump . "<br><br>";



	$sungokong = new ape("Kera Sakti");

	echo "Name: " . $sungokong->name . "<br>";
	echo "Legs: " . $sungokong->legs . "<br>";
	echo "Cold Blooded: " . $sungokong->cold_blooded . "<br>";
	echo "Yell: " . $sungokong->yell . "<br>";
	?>

</body>
</html>