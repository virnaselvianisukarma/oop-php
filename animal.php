<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title></title>
</head>
<body>
	<?php
	class animal
	{
		public $name;
		public $legs = 4;
		public $cold_blooded = "no";

		public function __construct($string){
			$this->name = $string;
		}
	}

// NB: Boleh juga menggunakan method get (get_name(), get_legs(), get_cold_blooded())

	?>
</body>
</html>